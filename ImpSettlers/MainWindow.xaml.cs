﻿using ImpSettlers.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImpSettlers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IController
    {
        private IModel model;

        public MainWindow()
        {
            this.model = Model.Model.Instance;

            InitializeComponent();
            //

            this.DataContext = this.model;

            // refresh
            this.model.StageChanged.Subscribe(delegate (IModel m)
            {
                // wymuszene aktualizacji bez zabawy w implementację IPropertyChanged
                //this.DataContext = new ModelDummyDecorator(m);
                //this.DataContext = m;
            });

            // game main loop
            Task.Run(async delegate ()
            {
                while (await this.model.Next(this)) { }
            });

            //
        }

        private ManualResetEvent isFinishFlag = new ManualResetEvent(false);

        public Task<bool> IsFinish(IModel model)
        {
            return Task.Run<bool>(delegate ()
            {
                this.isFinishFlag.WaitOne();
                this.isFinishFlag.Reset();
                return false;
            });
        }

        public Task<IFaction> PlayerFaction(IModel model)
        {

            return Dispatcher.InvokeAsync<IFaction>(delegate ()
            {
                var dlg = new SelectFactionDlg();

                dlg.Factions = model.AllFactions.ToList();

                dlg.ShowDialog();

                //return Task<IFaction>.FromResult(null);
                return dlg.Faction;
            }).Task;
        }

        public Task<RoundReview> ReviewRound(IModel model)
        {
            return Dispatcher.InvokeAsync<RoundReview>(delegate ()
            {
                var dlg = new RoundReviewDialog();

                dlg.Model = model;

                dlg.ShowDialog();


                return dlg.Review;
                //return dlg.TotalExpenses;
            }).Task;
        }

        public Task<IEnumerable<IAchievement>> SelectAchievements(IModel model, IEnumerable<IAchievement> achievements, IObservable<IStatus> st)
        {
            return Dispatcher.InvokeAsync<IEnumerable<IAchievement>>(delegate ()
            {
                var dlg = new BuyAchievementWindow();

                dlg.Input = new BuyAchievementWindow.InputData { Budget = model.Stock.VictoryPoints , Achievements = achievements};

                dlg.ShowDialog();


                return dlg.SelectedAchievements;
            }).Task;
        }

        public Task<IModifier> SelectEventModifier(IModel model, IEvent ec)
        {
            return Dispatcher.InvokeAsync<IModifier>(delegate ()
            {
                var dlg = new NewEventWindow();

                dlg.Event = ec;

                dlg.ShowDialog();


                return dlg.SelectedModifier;
            }).Task;
        }

        public Task<IProvince> SelectProvince(IModel model, IEnumerable<IProvince> provinces)
        {
            return Task<IProvince>.FromResult(provinces.First());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.isFinishFlag.Set();
        }
    }
}
