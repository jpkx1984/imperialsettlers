﻿using ImpSettlers.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ImpSettlers
{
    public enum ExpenseCategory
    {
        Locations = 0,
        Agreements = 1,
        Loot = 2,
        Cards = 3,
        Provinces = 4,
        Other = 5
    }

    public class DataTableInfo
    {
        public DataTable Table { get; set;  }
        public DataColumn CategoryColumn { get; set; }
        public DataColumn RoColumn { get; set; }
        public IDictionary<MaterialType, DataColumn> ProductColumns { get; set; }
        public DataRow SumRow { get; set; }
        public DataRow OtherRow { get; set; }
        public DataRow FinalRow { get; set; }
        public DataRow StorageRow { get; set; }
        public DataRow BalanceRow { get; set; }
    }

    public static class ModelExt
    {
        public static string ToDisplayString(this Enum cat)
        {
            return Utils.GetString(cat);
        }

        public static IDictionary<T, System.Data.DataColumn> AddEnumColumns<T>(System.Data.DataTable t, Func<T, string> captionFoo)
        {
            var cols = new Dictionary<T, System.Data.DataColumn>();

            foreach (var k in Utils.GetEnumKeys<T>())
            {
                var c = t.Columns.Add(k.ToString(), typeof(int));

                if (captionFoo != null)
                    c.Caption = captionFoo(k);

                cols[k] = c;
            }

            return cols;
        }

        public static DataTableInfo ToDataTable(this IDictionary<MaterialType, int> m, System.Data.DataTable t, bool fill)
        {
            var prodCols = AddEnumColumns<MaterialType>(t, x => x.ToDisplayString());

            if (fill)
            {
                var nr = t.NewRow();

                foreach (var kv in m)
                {
                    nr[prodCols[kv.Key]] = kv.Value;
                }

                t.Rows.Add(nr);
            }

            return new DataTableInfo
            {
                Table = t,
                ProductColumns = prodCols
            };
        }

        public static DataTableInfo ToDataTable(this IDictionary<MaterialType, int> m, bool fill)
        {
            DataTable t = new System.Data.DataTable();

            var d = m.ToDataTable(t, fill);

            t.AcceptChanges();

            return d;
        }

        public static DataTableInfo ToDataTable<T>(this IDictionary<T, IMaterials> m, bool includeSum = true)
        {
            System.Data.DataTable t = new System.Data.DataTable();

            var catColumn = t.Columns.Add("category", typeof(string));
            catColumn.Caption = Utils.GetString("s_category");

            var cols = AddEnumColumns<MaterialType>(t, x => x.ToDisplayString());

            foreach (System.Data.DataColumn dc in cols.Values) dc.DefaultValue = 0;

            foreach (var p in m)
            {
                var nr = t.NewRow();

                nr[catColumn] = Utils.GetString(p.Key);
                
                foreach (var pp in p.Value)
                {
                    nr[cols[pp.Key]] = pp.Value;
                }

                t.Rows.Add(nr);
            }

            DataRow sumRow = null;

            if (includeSum)
            {
                sumRow = t.NewRow();
                sumRow[catColumn] = Utils.GetString("s_total");

                t.Rows.Add(sumRow);

                t.RowChanged += (delegate (object s, DataRowChangeEventArgs a) {
                    if (sumRow == a.Row) return;

                    foreach (var dc in cols.Values)
                    {
                        var sum = t.Rows.Cast<System.Data.DataRow>()
                            .Where(r => r != sumRow)
                            .Select(r => r[dc])
                            .Where(x => x != null && !DBNull.Value.Equals(x) && x is int)
                            .Cast<int>()
                            .Sum();

                        sumRow[dc] = sum;
                    }
                });
            }

            t.AcceptChanges();

            return new DataTableInfo
            {
                Table = t,
                CategoryColumn = catColumn,
                ProductColumns = cols,
                SumRow = sumRow
            };
        }

        public static DataTableInfo ToDetailsDataTable<T>(this IDictionary<T, IMaterials> m)
        {
            var d = m.ToDataTable(true);


            DataTable tab = d.Table;
            foreach (System.Data.DataColumn dc in tab.Columns)
            {
                dc.ColumnName = dc.Caption;
            }

            var roColumn = tab.Columns.Add("RO", typeof(bool));
            foreach (System.Data.DataRow r in tab.Rows) r[roColumn] = true;

            var otherRow = tab.NewRow();
            otherRow[d.CategoryColumn] = Utils.GetString("s_others");
            otherRow[roColumn] = false;

            tab.Rows.InsertAt(otherRow, tab.Rows.IndexOf(d.SumRow));

            tab.AcceptChanges();

            return new DataTableInfo
            {
                Table = tab,
                CategoryColumn = d.CategoryColumn,
                ProductColumns = d.ProductColumns,
                SumRow = d.SumRow,
                RoColumn = roColumn,
                OtherRow = otherRow
            };
        }

        public static DataTableInfo ProductionDetails(this IModel model)
        {

            var prodDetails = new Dictionary<ProductionCategory, IMaterials>();
            prodDetails[ProductionCategory.Faction] = model.PlayerFaction.BaseProduction;

            var prodMods = model.Modifiers
                .Where(m => m.Status == ModifierStatus.Present && m.Modifier is IStorageModifier)
                .Cast<IStorageModifierInstance>()
                .Where(sm => ((IStorageModifier)sm.Modifier).Delta.Production != null).ToList();

            var provinceProd = prodMods
                .Where(m => m.Origin is IProvince)
                .Aggregate((IMaterials)new MaterialsImpl(), (a, v) => a.Add(((IStorageModifier)v.Modifier).Delta.Production));

            var effProd = prodMods
                .Where(m => !(m.Origin is IProvince))
                .Aggregate((IMaterials)new MaterialsImpl(), (a, v) => a.Add(((IStorageModifier)v.Modifier).Delta.Production));


            prodDetails[ProductionCategory.Provinces] = provinceProd;
            prodDetails[ProductionCategory.Effects] = effProd;


            var d = prodDetails.ToDetailsDataTable();

            return d;
        }


        public static DataTableInfo ExpenseDetails(this IModel model)
        {
            var expenseDetails = new Dictionary<ProductionCategory, IMaterials>();

            var d = expenseDetails.ToDetailsDataTable();
            System.Data.DataTable t = d.Table;


            return d;
        }

        public static DataTableInfo StockDetails(this IModel model)
        {
            var stockDetails = new Dictionary<ProductionCategory, IMaterials>();

            var d = model.Stock.ToDataTable(true);

            System.Data.DataTable dt = d.Table;
            foreach (System.Data.DataColumn cd in dt.Columns) cd.ColumnName = cd.Caption;


            return d;
        }

        public static void FinalStockRecalc(IModel model, DataTableInfo finalStock, DataTableInfo production, DataTableInfo expenses)
        {
            foreach (var mt in model.Capacity)
            {
                var col = finalStock.ProductColumns[mt.Key];
                var prodCol = production.ProductColumns[mt.Key];
                var expCol = expenses.ProductColumns[mt.Key];
                int b = ((int)production.SumRow[prodCol]) + model.Stock[mt.Key] - (int)expenses.SumRow[expCol];
                finalStock.BalanceRow[col] = b;
                //finalStock.StorageRow[col] = mt.Value;
                finalStock.FinalRow[col] = Math.Min(mt.Value, b);
            }
        }

        public static DataTableInfo FinalStockDetails(this IModel model, DataTableInfo production, DataTableInfo expenses)
        {
            var stockDetails = new Dictionary<ProductionCategory, IMaterials>();
            var tab = new DataTable();
            tab.Columns.Add("-", typeof(string));
            var d = model.Stock.ToDataTable(tab, false);


            System.Data.DataTable dt = d.Table;
            foreach (System.Data.DataColumn cd in dt.Columns) cd.ColumnName = cd.Caption;

            d.CategoryColumn = tab.Columns[0];
            d.RoColumn = d.Table.Columns.Add("RO", typeof(bool));

            var balanceRow = d.Table.NewRow();
            balanceRow[d.CategoryColumn] = Utils.GetString("s_gross");
            balanceRow[d.RoColumn] = true;

            var storageRow = d.Table.NewRow();
            storageRow[d.CategoryColumn] = Utils.GetString("s_storage");
            storageRow[d.RoColumn] = false;
            foreach (var mt in model.Capacity.Keys)
            {
                storageRow[d.ProductColumns[mt]] = model.Capacity[mt];
            }

            var finalRow = d.Table.NewRow();
            finalRow[d.CategoryColumn] = Utils.GetString("s_finally");
            finalRow[d.RoColumn] = true;

            d.FinalRow = finalRow;
            d.StorageRow = storageRow;
            d.BalanceRow = balanceRow;

            FinalStockRecalc(model, d, production, expenses);

            d.Table.Rows.Add(balanceRow);
            d.Table.Rows.Add(storageRow);
            d.Table.Rows.Add(finalRow);
            d.Table.AcceptChanges();



            production.Table.RowChanged += delegate (object sender, DataRowChangeEventArgs e)
            {
                FinalStockRecalc(model, d, production, expenses);
            };

            expenses.Table.RowChanged += delegate (object sender, DataRowChangeEventArgs e)
            {
                FinalStockRecalc(model, d, production, expenses);
            };

            tab.RowChanged += delegate (object sender, DataRowChangeEventArgs e)
            {
                if (e.Row == storageRow)
                    FinalStockRecalc(model, d, production, expenses);
            };

            return d;
        }

        private static void Tab_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Interaction logic for RoundReviewDialog.xaml
    /// </summary>
    public partial class RoundReviewDialog : Window
    {
        private IModel model;
        private DataTableInfo currentStock = new DataTableInfo();
        private DataTableInfo currentProduction = new DataTableInfo();
        private DataTableInfo currentExpenses = new DataTableInfo();
        private DataTableInfo finalStock = new DataTableInfo();

        public bool? IsVictory { get; set; }

        public IModel Model
        {
            get
            {
                return this.model;
            }

            set
            {
                this.model = value;
                //
                this.currentStock = model.StockDetails();
                this.currentProduction = model.ProductionDetails();
                this.currentExpenses = model.ExpenseDetails();
                this.finalStock = model.FinalStockDetails(this.currentProduction, this.currentExpenses);
                //

                //
            }
        }

        public System.Data.DataTable CurrentStock
        {
            get
            {
                return this.currentStock?.Table;
            }
        }

        public System.Data.DataTable CurrentProduction
        {
            get
            {
                return this.currentProduction?.Table;
            }
        }

        public string RoundDescription
        {
            get
            {
                return $"{Utils.GetString("s_round")} {this.Model.Round + 1} / 4";
            }
        }

        public IStorage Totals
        {
            get
            {
                var expenses = new MaterialsImpl();
                var production = new MaterialsImpl();
                var stock = new MaterialsImpl();
                var capacity = new MaterialsImpl();

                foreach (var k in expenses.Keys)
                {
                    expenses[k] = (int)this.currentExpenses.SumRow[this.currentExpenses.ProductColumns[k]];
                    production[k] = (int)this.currentProduction.SumRow[this.currentProduction.ProductColumns[k]];
                    stock[k] = (int)this.finalStock.FinalRow[this.finalStock.ProductColumns[k]];
                    capacity[k] = (int)this.finalStock.StorageRow[this.finalStock.ProductColumns[k]];
                }

                return new StorageImpl(expenses: expenses, production: production, stock: stock, capacity: capacity);
            }
        }

        public RoundReview Review
        {
            get
            {
                return new RoundReview { Storage = Totals, Victory = this.IsVictory };
            }
        }

        public System.Data.DataTable CurrentExpenses
        {
            get
            {
                return this.currentExpenses?.Table;
            }
        }

        public System.Data.DataTable FinalStock
        {
            get
            {
                return this.finalStock?.Table;
            }
        }

        public RoundReviewDialog()
        {
            InitializeComponent();
        }

        private void prodGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "RO")
                e.Cancel = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
