﻿using ImpSettlers.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImpSettlers
{

    /// <summary>
    /// Interaction logic for BuyAchievementWindow.xaml
    /// </summary>
    public partial class BuyAchievementWindow : Window
    {
        public class InputData
        {
            public int Budget { get; set; }
            public IEnumerable<IAchievement> Achievements { get; set; }
        }

        private BuyAchievementWindow.InputData inp =  new InputData { Budget = 0, Achievements = new List<IAchievement>() }; 
        public BuyAchievementWindow.InputData Input
        {
            get
            {
                return this.inp;
            }

            set
            {
                this.inp = value;
                this.DataContext = value;
            }
        }

        public IEnumerable<IAchievement> SelectedAchievements { get; set; }

        public BuyAchievementWindow()
        {
            this.Input = new InputData { Budget = 0, Achievements = new List<IAchievement>() };
            this.SelectedAchievements = new List<IAchievement>();

            InitializeComponent();

            this.DataContext = this.Input;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
