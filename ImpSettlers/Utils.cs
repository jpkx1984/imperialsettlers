﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpSettlers
{
    public static class Utils
    {
        public static string GetString(string s)
        {
            string x = ImpSettlers.Properties.Resources.ResourceManager.GetString(s);
            return string.IsNullOrEmpty(x) ? s : x;
        }

        public static string GetString(object t)
        {
            return GetString(t.GetType().Name + "_" + t.ToString());
        }

        public static IEnumerable<T> GetEnumKeys<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }
    }
}
