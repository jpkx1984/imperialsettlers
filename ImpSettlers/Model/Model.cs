﻿using ImpSettlers.Model.Modifiers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpSettlers.Model
{
    public interface IStatus
    {
        bool Ok { get; }
        bool Retry { get; }
        string Message { get; }
    }

    public class StatusImpl : IStatus
    {
        private bool ok;
        private bool retry;

        string msg = "";

        public bool Ok { get { return this.ok; } }
        public bool Retry { get { return this.retry; } }
        public string Message { get { return this.msg; } }

        public StatusImpl(bool ok, bool retry, string msg)
        {
            this.ok = ok;
            this.retry = retry;
            this.msg = msg;
        }
    }

    public enum MaterialType
    {
        Workers,
        Food,
        Timber,
        Stone,
        Gold,
        Swords,
        VictoryPoints,
        Cards
    }

    public enum ProductionCategory
    {
        Faction = 0,
        Provinces = 1,
        Effects = 2,
        Other = 3
    }

    public interface IMaterials : IDictionary<MaterialType, int>
    {
        int Workers { get; }
        int Food { get; }
        int Timber { get; }
        int Stone { get; }
        int Gold { get; }
        int Swords { get; }
        int VictoryPoints { get; }
        int Cards { get; }

        IMaterials Add(IMaterials from);
        IMaterials Subtract(IMaterials from);
        IMaterials Cut(IMaterials from);
    }

    public class MaterialsImpl : IMaterials
    {
        private int food = 0;
        private int workers = 0;
        private int timber = 0;
        private int stone = 0;
        private int gold = 0;
        private int swords = 0;
        private int vp = 0;
        private int cards = 0;

        public int Food
        {
            get
            {
                return this.food;
            }
        }

        public int Gold
        {
            get
            {
                return this.gold;
            }
        }

        public int Stone
        {
            get
            {
                return this.stone;
            }
        }

        public int Swords
        {
            get
            {
                return this.swords;
            }
        }

        public int Timber
        {
            get
            {
                return this.timber;
            }
        }

        public int VictoryPoints
        {
            get
            {
                return this.vp;
            }
        }

        public int Workers
        {
            get
            {
                return this.workers;
            }
        }

        public int Cards
        {
            get
            {
                return this.cards;
            }
        }

        public ICollection<MaterialType> Keys
        {
            get
            {
                return Enum.GetValues(typeof(MaterialType)).Cast<MaterialType>().ToList();
            }
        }

        public ICollection<int> Values
        {
            get
            {
                return new int[] { this.Food, this.Gold, this.Stone, this.Swords, this.Timber, this.VictoryPoints, this.Workers, this.Cards };
            }
        }

        public int Count
        {
            get
            {
                return this.Keys.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public int this[MaterialType key]
        {
            get
            {
                switch (key)
                {
                    case MaterialType.Food:
                        return this.Food;
                    case MaterialType.Gold:
                        return this.Gold;
                    case MaterialType.Stone:
                        return this.Stone;
                    case MaterialType.Swords:
                        return this.Swords;
                    case MaterialType.Timber:
                        return this.Timber;
                    case MaterialType.VictoryPoints:
                        return this.VictoryPoints;
                    case MaterialType.Workers:
                        return this.Workers;
                    case MaterialType.Cards:
                        return this.Cards;
                }

                return 0;
            }

            set
            {
                switch (key)
                {
                    case MaterialType.Food:
                        this.food = value;
                        break;
                    case MaterialType.Gold:
                        this.gold = value;
                        break;
                    case MaterialType.Stone:
                        this.stone = value;
                        break;
                    case MaterialType.Swords:
                        this.swords = value;
                        break;
                    case MaterialType.Timber:
                         this.timber = value;
                        break;
                    case MaterialType.VictoryPoints:
                        this.vp = value;
                        break;
                    case MaterialType.Workers:
                        this.workers = value;
                        break;
                    case MaterialType.Cards:
                        this.cards = value;
                        break;
                }
            }
        }

        public IMaterials Add(IMaterials from)
        {
            return new MaterialsImpl {
                food = this.Food + from.Food,
                gold = this.Gold + from.Gold,
                stone = this.Stone + from.Stone,
                swords = this.Swords + from.Swords,
                timber = this.Timber + from.Timber,
                vp = this.VictoryPoints + from.VictoryPoints,
                cards = this.Cards + from.Cards
            };
        }

        public IMaterials Subtract(IMaterials from)
        {
            return new MaterialsImpl
            {
                food = this.Food - from.Food,
                gold = this.Gold - from.Gold,
                stone = this.Stone - from.Stone,
                swords = this.Swords - from.Swords,
                timber = this.Timber - from.Timber,
                vp = this.VictoryPoints - from.VictoryPoints,
                cards = this.Cards - from.Cards
            };
        }

        public IMaterials Cut(IMaterials from)
        {
            return new MaterialsImpl
            {
                food = Math.Min(this.Food, from.Food),
                gold = Math.Min(this.Gold, from.Gold),
                stone = Math.Min(this.Stone, from.Stone),
                swords = Math.Min(this.Swords, from.Swords),
                timber = Math.Min(this.Timber, from.Timber)
            };
        }

        public bool ContainsKey(MaterialType key)
        {
            return true;
        }

        public void Add(MaterialType key, int value)
        {
            throw new NotImplementedException();
        }

        public bool Remove(MaterialType key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(MaterialType key, out int value)
        {
            value = this[key];
            return true;
        }

        public void Add(KeyValuePair<MaterialType, int> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<MaterialType, int> item)
        {
            return this[item.Key] == item.Value;
        }

        public void CopyTo(KeyValuePair<MaterialType, int>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<MaterialType, int> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<MaterialType, int>> GetEnumerator()
        {
            return this.Keys.Select(k => new KeyValuePair<MaterialType, int>(k, this[k])).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.Keys.Select(k => new KeyValuePair<MaterialType, int>(k, this[k])).GetEnumerator();
        }

        public MaterialsImpl(int? food = null, int? gold = null, int? stone = null, int? swords = null, int? timber = null)
        {
            this.food = food.GetValueOrDefault(0);
            this.gold = gold.GetValueOrDefault(0);
            this.stone = stone.GetValueOrDefault(0);
            this.swords = swords.GetValueOrDefault(0);
            this.timber = timber.GetValueOrDefault(0);
        }
    }

    public interface IStorage
    {
        IMaterials Stock { get; }
        IMaterials Capacity { get; }
        IMaterials Production { get; }
        IMaterials Expenses { get; }
    }


    public class StorageImpl : IStorage
    {
        private IMaterials capacity = new MaterialsImpl();
        private IMaterials production = new MaterialsImpl();
        private IMaterials stock = new MaterialsImpl();
        private IMaterials expenses = new MaterialsImpl();

        public IMaterials Capacity
        {
            get
            {
                return this.capacity;
            }
        }

        public IMaterials Production
        {
            get
            {
                return this.production;
            }
        }

        public IMaterials Stock
        {
            get
            {
                return this.stock;
            }
        }

        public IMaterials Expenses
        {
            get
            {
                return this.expenses;
            }
        }

        public StorageImpl(IMaterials stock = null, IMaterials production = null, IMaterials capacity = null, IMaterials expenses = null)
        {
            if (stock != null) this.stock = stock;
            if (production != null) this.production = production;
            if (capacity != null) this.capacity = capacity;
            if (expenses != null) this.expenses = expenses;
        }
    }

    public enum Phase
    {
        Scouting = 0,
        Production = 1,
        Action = 2,
        Cleaning = 3,
        Attack = 4
    }

    static class PhaseExtension
    {
        public static Phase Cycle(this Phase ph)
        {
            return (Phase)((((int)ph) + 1) % 5);
        }
    }

    public interface IModifier
    {
        string Description { get; }
        IModifierInstance NewModifier(IModel model, object origin, Func<IModifierInstance, IModel, ModifierStatus> activator = null);
        bool IsApplicable(IModel model);
    }

    public enum ModifierStatus
    {
        Future,
        Present,
        Past,
        Invalid
    }

    public interface IModifierInstance
    {
        IModifier Modifier { get; }
        object Origin { get;  }
        ModifierStatus Status { get; }
        void Consume(IModel model, IController controller);
    }

    public static class ModifierActivators
    {
        public static Func<IModifierInstance, IModel, ModifierStatus> MakeNextRoundActivator(IModel model, bool persistent = false)
        {
            return delegate (IModifierInstance mi, IModel md) {
                var round = model.Round;

                if (round == md.Round)
                    return ModifierStatus.Future;
                else if (round != md.Round)
                {
                    if (mi.Status == ModifierStatus.Present)
                    {
                        return persistent ? ModifierStatus.Present : ModifierStatus.Past;
                    }

                    return mi.Status;
                }

                return mi.Status;
            };
        }

        public static Func<IModifierInstance, IModel, ModifierStatus> MakeThisRoundActivator(IModel model, bool persistent = false)
        {
            return delegate (IModifierInstance mi, IModel md) {
                var round = model.Round;

                if (round == md.Round)
                    return ModifierStatus.Present;
                else if (round != md.Round)
                {
                    if (mi.Status == ModifierStatus.Present)
                    {
                        return persistent ? ModifierStatus.Present : ModifierStatus.Past;
                    }

                    return mi.Status;
                }

                return mi.Status;
            };
        }
    }

    public abstract class ModifierBase<T> : IModifier where T : IModifierInstance
    {
        private string description;
        protected Func<IModifierInstance, IModel, ModifierStatus> defaultActivator;


        public ModifierBase(string description, Func<IModifierInstance, IModel, ModifierStatus> defaultActivator = null)
        {
            this.description = description;
            this.defaultActivator = defaultActivator;
        }

        public string Description
        {
            get
            {
                return this.description;
            }
        }

        public virtual bool IsApplicable(IModel model)
        {
            return true;
        }

        protected virtual IModifierInstance NewModifierInternal(IModel model, object origin, Func<IModifierInstance, IModel, ModifierStatus> activator)
        {
            var constr = typeof(T).GetConstructors().Where(c => c.IsPublic && c.GetParameters().Length == 3).First();
            return (IModifierInstance)constr.Invoke(new object[] { this, origin, activator});
        }

        public virtual IModifierInstance NewModifier(IModel model, object origin, Func<IModifierInstance, IModel, ModifierStatus> activator = null)
        {
            if (activator == null)
                activator = this.defaultActivator;

            return NewModifierInternal(model, origin, activator);
        }
    }

    public abstract class ModifierInstanceBase : IModifierInstance
    {
        private IModifier modifier;
        private ModifierStatus status = ModifierStatus.Future;
        private object origin;
        private Func<IModifierInstance, IModel, ModifierStatus> activator;

        protected Func<IModifierInstance, IModel, ModifierStatus> Activator => this.activator;

        public IModifier Modifier
        {
            get
            {
                return this.modifier;
            }
        }

        public virtual ModifierStatus Status
        {
            get
            {
                return this.status;
            }

            protected set
            {
                this.status = value;
            }
        }

        public object Origin
        {
            get
            {
                return this.origin;
            }
        }
        
        public virtual void Consume(IModel model, IController controller)
        {
            if (this.Origin is IProvince && !model.Provinces.Contains((IProvince)this.Origin))
            {
                this.status = ModifierStatus.Invalid;
                return;
            }

            if (this.Origin is IAchievement && !model.Achievements.Contains((IAchievement)this.Origin))
            {
                this.status = ModifierStatus.Invalid;
                return;
            }


            if (this.Activator != null)
            {
                this.status = this.Activator(this, model);
            }
            else
            {
                if (this.status == ModifierStatus.Future)
                    this.status = ModifierStatus.Present;
                else if (this.status == ModifierStatus.Present)
                    this.status = ModifierStatus.Past;
            }
        }

        public ModifierInstanceBase(IModifier modifier, object origin, Func<IModifierInstance, IModel, ModifierStatus> activator)
        {
            this.modifier = modifier;
            this.origin = origin;
            this.activator = activator;
        }
    }

    public interface IStorageModifier : IModifier
    {
        IStorage Delta { get; set;  }
    }

    public interface IStorageModifierInstance : IModifierInstance
    {
    }

    public interface IProvinceControlCostModifier : IModifier
    {
        int Delta { get;  }
    }

    public interface ISkipEventsModifier : IModifier
    {

    }

    public interface IProvince
    {
        string Name { get; }
        string Description { get;  }
        IMaterials Upkeep { get; }
        int ControlCost { get;  }
        IEnumerable<IModifier> AvailableModifiers { get;  }
    }

    public class IProvinceImpl : IProvince
    {
        private List<IModifier> modifiers = new List<IModifier>();

        public IEnumerable<IModifier> AvailableModifiers
        {
            get
            {
                return modifiers;
            }
        }

        private int controlCost = 0;

        public int ControlCost
        {
            get
            {
                return this.controlCost;
            }
        }

        private string description = "";

        public string Description
        {
            get
            {
                return this.description;
            }
        }

        private string name = "";

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        private IMaterials upkeep = new MaterialsImpl();

        public IMaterials Upkeep
        {
            get
            {
                return this.upkeep;
            }
        }

        public IProvinceImpl(string name, string description, int cc, IMaterials uk, IEnumerable<IModifier> mods = null)
        {
            this.name = name;
            this.description = description;
            this.controlCost = cc;

            if (uk == null)
            {
                uk = new MaterialsImpl();
            }
            this.upkeep = uk;

            if (mods != null)
                this.modifiers = mods.ToList();
        }

        public static IProvinceImpl NewProvince(string name, string description, int cc, IMaterials uk, params IModifier[] mods)
        {
            return new IProvinceImpl(name, description, cc, uk, mods);
        }
    } 

    public interface IEvent
    {
        string Name { get; }
        string Description { get; }
        IModifier Effect { get; }
        IEnumerable<IModifier> ExtraEffects { get;  }
    }

    public class EventImpl : IEvent
    {
        private string name;
        private string description;
        private IModifier effect;
        private List<IModifier> extraEffects;


        public string Description
        {
            get
            {
                return this.description;
            }
        }

        public IModifier Effect
        {
            get
            {
                return this.effect;
            }
        }

        public IEnumerable<IModifier> ExtraEffects
        {
            get
            {
                return this.extraEffects;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public EventImpl(string name, string description, IModifier effect, IEnumerable<IModifier> extraEffects)
        {
            this.name = name;
            this.description = description;
            this.effect = effect;

            if (extraEffects == null) extraEffects = new List<IModifier>();
            this.extraEffects = extraEffects.ToList();
        }
    }

    public interface IAchievement
    {
        int Cost { get;  }
        bool IsAvailable(Model m);
        string Name { get;  }
        string Description { get; }
    }

    public class AchievementImpl : IAchievement
    {
        private int cost = 0;

        public int Cost
        {
            get
            {
                return this.cost;
            }
        }

        private string name = "";

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        private string description = "";

        public string Description
        {
            get
            {
                return this.description;
            }
        }

        public bool IsAvailable(Model m)
        {

            return this.availFoo != null ? this.availFoo(m) : false;
        }

        private Func<IModel, bool> availFoo;

        public AchievementImpl(string name, string description, int cost, Func<IModel, bool> availFoo)
        {
            this.cost = cost;
            this.availFoo = availFoo;
            this.description = description;
            this.name = name;
        }
    }

    public interface IFaction
    {
        string Name { get; }
        IMaterials BaseProduction { get; }
    }

    public class FactionImpl : IFaction
    {
        private string name;
        private IMaterials baseProduction = new MaterialsImpl();

        public IMaterials BaseProduction
        {
            get
            {
                return this.baseProduction;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public FactionImpl(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public interface IModelData
    {
        IMaterials Production { get;  }
        IMaterials Stock { get; }
        IMaterials Capacity { get; }
        IFaction PlayerFaction { get; }
        IFaction BorderFaction { get; }
        //IStorage Storage { get; }
        int Campaign { get; }
        int Turn { get; }
        int Round { get; }
        bool IsLastRound { get;  }
        IEnumerable<IProvince> Provinces { get; }
        IEnumerable<IEvent> Events { get; }
        IEnumerable<IAchievement> Achievements { get; }
        IEnumerable<IModifierInstance> Modifiers { get; }
    }

    public interface IModel : IModelData, INotifyPropertyChanged
    {
        IEnumerable<IFaction> AllFactions { get; }
        IEnumerable<IEvent> AllEvents { get; }
        IEnumerable<IProvince> AllProvinces { get; }
        IEnumerable<IAchievement> AllAchievements { get; }
        Task<bool> Next(IController dataProvider);
        IModelStage Stage { get;  }

        IObservable<IModel> StageChanged { get; }
    }

    public enum StageEnum
    {
        Init,
        Events,
        Rounds,
        VictoryPointsCalc,
        Provinces,
        VictoryPointsSpending,
        Results,
        None
    }

    public class RoundReview
    {
        public IStorage Storage { get; set; }
        public bool? Victory { get; set; }
    }

    public interface IController
    {
        Task<IFaction> PlayerFaction(IModel model);
        Task<IEnumerable<IAchievement>> SelectAchievements(IModel model, IEnumerable<IAchievement> achievements, IObservable<IStatus> st);
        Task<IModifier> SelectEventModifier(IModel model, IEvent ec);
        Task<IProvince> SelectProvince(IModel model, IEnumerable<IProvince> provinces);
        Task<RoundReview> ReviewRound(IModel model);
        Task<bool> IsFinish(IModel model);
    }

    public interface IModelStage : IModelData
    {
        Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller);
    }

    public abstract class ModelDataBase : IModelData
    {
        protected readonly static Random rnd = new Random();
        protected IFaction playerFaction = null;
        protected IFaction borderFaction = null;
        protected HashSet<IProvince> provinces = new HashSet<IProvince>();
        protected HashSet<IEvent> events = new HashSet<IEvent>();
        protected int campaign = 0;
        protected int turn = 0;
        protected int round = 0;
        protected HashSet<IAchievement> achievements = new HashSet<IAchievement>();
        protected List<IModifierInstance> modifiers = new List<IModifierInstance>();

        protected IMaterials capacity = new MaterialsImpl(20, 20, 20, 20, 20);
        //protected IMaterials production = new MaterialsImpl();
        protected IMaterials stock = new MaterialsImpl();

        private IEnumerable<T> FindPresentModifiers<T>() where T : IModifier
        {
            return this.Modifiers
                    .Where(m => m.Status == ModifierStatus.Present && m.Modifier is T)
                    .Select(m => m.Modifier)
                    .Cast<T>();
        }

        public IMaterials Capacity
        {
            get
            {
                IMaterials capacity = this.capacity;

                foreach (var m in FindPresentModifiers<IStorageModifier>())
                    capacity = capacity.Add(m.Delta.Capacity);

                return capacity;
            }
        }

        public IMaterials Production
        {
            get
            {
                var prod = this?.PlayerFaction?.BaseProduction;

                if (prod == null)
                    prod = new MaterialsImpl();

                foreach (var m in FindPresentModifiers<IStorageModifier>())
                    prod = prod.Add(m.Delta.Production);
                
                return prod;
            }
        }

        public IMaterials Stock
        {
            get
            {
                IMaterials stock = this.stock;

                foreach (var m in FindPresentModifiers<IStorageModifier>())
                    stock = stock.Add(m.Delta.Stock);

                return stock;
            }
        }

        public IEnumerable<IAchievement> Achievements
        {
            get
            {
                return this.achievements;
            }
        }

        public IFaction BorderFaction
        {
            get
            {
                return this.borderFaction;
            }
        }

        public int Campaign
        {
            get
            {
                return this.campaign;
            }
        }

        public IEnumerable<IEvent> Events
        {
            get
            {
                return this.events;
            }
        }

        public IFaction PlayerFaction
        {
            get
            {
                return this.playerFaction;
            }

            set
            {
                this.playerFaction = value;
            }
        }

        public IEnumerable<IProvince> Provinces
        {
            get
            {
                return this.provinces;
            }
        }

        public int Round
        {
            get
            {
                return this.round;
            }
        }

        public bool IsLastRound
        {
            get
            {
                return this.Round == 3;
            }
        }

        public int Turn
        {
            get
            {
                return this.turn;
            }
        }

        public IEnumerable<IModifierInstance> Modifiers
        {
            get
            {
                return this.modifiers;
            }
        }
    }

    public class StageBase: ModelDataBase, IModelStage
    {
        protected void From(IModelData md)
        {
            foreach (var t in md.Achievements) this.achievements.Add(t);
            foreach (var t in md.Provinces) this.provinces.Add(t);
            foreach (var t in md.Events) this.events.Add(t);
            foreach (var t in md.Modifiers) this.modifiers.Add(t);

            this.turn = md.Turn;
            this.borderFaction = md.BorderFaction;
            this.playerFaction = md.PlayerFaction;
            this.campaign = md.Campaign;
            //this.production = md.Production;
            this.capacity = md.Capacity;
            this.stock = md.Stock;
        }

        public virtual Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller)
        {
            if (prevStage != null)
                this.From(prevStage);

            return Task.FromResult<IModelStage>(this);
        }
    }

    public class InitStage : StageBase
    {
        public override async Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller)
        {
            await base.Next(prevStage, model, controller);

            this.turn = 0;
            this.round = 0;

            this.playerFaction = await controller.PlayerFaction(model);

            // losowanie frakcji granicznej
            var fcs = model.AllFactions.Where(f => !f.Equals(this.PlayerFaction)).ToList();
            this.borderFaction = fcs[ModelDataBase.rnd.Next(0, fcs.Count)];
            
            return new EventsStage();
        }
    }

    public class EventsStage : StageBase
    {
        public override async Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller)
        {
            await base.Next(prevStage, model, controller);

            if (this.Turn == 0 || this.Provinces.Count() == 0)
                return new PlayStage();

            // losowanie zdarzenia
            var allEvents = model.AllEvents.ToList();

            allEvents.RemoveAll(ev => this.Events.Contains(ev));
            IEvent newEvent = null;

            if (allEvents.Count > 0)
            {
                newEvent = allEvents[ModelDataBase.rnd.Next(0, allEvents.Count)];
            }

            // wybór efektu

            if (newEvent != null)
            {
                IModifier mod = await controller.SelectEventModifier(model, newEvent);

                this.events.Add(newEvent);

                if (!mod.IsApplicable(model))
                {
                    throw new InvalidOperationException();
                }

                this.modifiers.Add(mod.NewModifier(model, newEvent));
            }

            return new PlayStage();
        }
    }

    public class PlayStage : StageBase
    {
        public override async Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller)
        {
            await base.Next(prevStage, model, controller);
            //

            foreach (var m in this.modifiers)
            {
                m.Consume(model, controller);
            }
            //

            RoundReview review = await controller.ReviewRound(model);

            this.capacity = review.Storage.Capacity;

            this.stock = review.Storage.Stock;
            this.stock = this.stock.Cut(this.Capacity);

            //
            ++this.round;

            if (model.Round < 4)
            {
                return (IModelStage)this;
            } else
            {
                // losowanie prowincji

                var otherProvinces = model.AllProvinces
                    .Where(p => !model.Provinces.Contains(p)).ToList();

                if (otherProvinces.Count > 0)
                {
                    var newProvince = otherProvinces[ModelDataBase.rnd.Next(otherProvinces.Count)];

                    this.provinces.Add(newProvince);

                    foreach (IModifier mod in newProvince.AvailableModifiers)
                    {
                        this.modifiers.Add(mod.NewModifier(model, newProvince));
                    }
                }

                return (new AchievementsStage());
            }
        }
    }

    public class AchievementsStage : StageBase
    {
        public override async Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller)
        {
            await base.Next(prevStage, model, controller);

            Task<bool> b = new Task<bool>(delegate ()
            {

                return true;
            });

            System.Reactive.Subjects.Subject<IStatus> statusSub = new System.Reactive.Subjects.Subject<IStatus>();


            bool wrong = true;
            List<IAchievement> achs = null;

            while (wrong)
            {
                achs = (await controller.SelectAchievements(model, model.AllAchievements.Where(a => !this.achievements.Contains(a)), statusSub)).ToList();

                int achCost = 0;

                wrong = false;

                

                foreach (IAchievement ach in achs)
                {
                    achCost += ach.Cost;

                    if (achCost > this.Stock.VictoryPoints)
                    {
                        wrong = true;
                        statusSub.OnNext(new StatusImpl(false, true, "Zbyt drogo"));
                    }
                }
            }

            statusSub.OnNext(new StatusImpl(true, false, "OK"));
            statusSub.OnCompleted();

            if (achs != null)
            {
                achs.Do((a) => this.achievements.Add(a));
            }

            return new ResultsStage();
        }
    }

    public class ResultsStage : StageBase
    {
        public override async Task<IModelStage> Next(IModelStage prevStage, IModel model, IController controller)
        {
            await base.Next(prevStage, model, controller);

            var isFinish = await controller.IsFinish(model);

            return isFinish ? null : new EventsStage();
        }
    }

    public class ModelDummyDecorator : IModel
    {
        private IModel model;

        public ModelDummyDecorator(IModel m)
        {
            this.model = m;
        }

        public IEnumerable<IAchievement> Achievements
        {
            get
            {
                return this.model.Achievements;
            }
        }

        public IEnumerable<IAchievement> AllAchievements
        {
            get
            {
                return this.model.AllAchievements;
            }
        }

        public IEnumerable<IEvent> AllEvents
        {
            get
            {
                return this.model.AllEvents;
            }
        }

        public IEnumerable<IFaction> AllFactions
        {
            get
            {
                return this.model.AllFactions;
            }
        }

        public IEnumerable<IProvince> AllProvinces
        {
            get
            {
                return this.model.AllProvinces;
            }
        }

        public IFaction BorderFaction
        {
            get
            {
                return this.model.BorderFaction;
            }
        }

        public int Campaign
        {
            get
            {
                return this.model.Campaign;
            }
        }

        public IMaterials Capacity
        {
            get
            {
                return this.model.Capacity;
            }
        }

        public IEnumerable<IEvent> Events
        {
            get
            {
                return this.model.Events;
            }
        }

        public IEnumerable<IModifierInstance> Modifiers
        {
            get
            {
                return this.model.Modifiers;
            }
        }

        public IFaction PlayerFaction
        {
            get
            {
                return this.model.PlayerFaction;
            }
        }

        public IMaterials Production
        {
            get
            {
                return this.model.Production;
            }
        }

        public IEnumerable<IProvince> Provinces
        {
            get
            {
                return this.model.Provinces;
            }
        }

        public int Round
        {
            get
            {
                return this.model.Round;
            }
        }

        public bool IsLastRound
        {
            get
            {
                return this.model.IsLastRound;
            }
        }

        public IModelStage Stage
        {
            get
            {
                return this.model.Stage;
            }
        }

        public IObservable<IModel> StageChanged
        {
            get
            {
                return this.model.StageChanged;
            }
        }

        public IMaterials Stock
        {
            get
            {
                return this.model.Stock;
            }
        }

        public int Turn
        {
            get
            {
                return this.model.Turn;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Task<bool> Next(IController dataProvider)
        {
            return this.Next(dataProvider);
        }
    }

    public class Model : IModel
    {
        private static Model instance = null;

        public static IModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Model();

                    instance.allFactions = new List<IFaction>();
                    instance.allFactions.Add(new FactionImpl("Rzymianie"));
                    instance.allFactions.Add(new FactionImpl("Japończycy"));

                    instance.allProvinces.Add(IProvinceImpl.NewProvince("Prowincja 1",
                        "Opis", 1,
                        new MaterialsImpl(),
                        new StorageModifier("Na stałe zwiększ produkcję o 1 Kamień.", 
                            new StorageImpl(production: new MaterialsImpl(stone: 1)),
                            ModifierActivators.MakeThisRoundActivator(instance, true))));


                    instance.allEvents.Add(new EventImpl("Wydarzenie1", "wydarzenie1 opis", null, null));

                    instance.allAchievements.Add(new AchievementImpl("Osiagnięcie 1", "Opis", 20, (m) => true));
                }

                return instance;
            }
        }

        private IModelStage stage = new InitStage();

        public IModelStage Stage
        {
            get { return this.stage; }
        }

        private List<IFaction> allFactions = null;

        public IEnumerable<IFaction> AllFactions
        {
            get
            {
                if (this.allFactions == null)
                {
                    this.allFactions = new List<IFaction>();
                }

                return this.allFactions;
            }
        }

        private List<IEvent> allEvents = new List<IEvent>();

        public IEnumerable<IEvent> AllEvents
        {
            get
            {
                return this.allEvents;
            }
        }

        public IFaction PlayerFaction
        {
            get
            {
                return this.Stage.PlayerFaction;
            }
        }

        public IFaction BorderFaction
        {
            get
            {
                return this.Stage.BorderFaction;
            }
        }

        public int Campaign
        {
            get
            {
                return this.Stage.Campaign;
            }
        }

        public int Round
        {
            get
            {
                return this.Stage.Round;
            }
        }

        public bool IsLastRound
        {
            get
            {
                return this.Stage.IsLastRound;
            }
        }

        public IEnumerable<IProvince> Provinces
        {
            get
            {
                return this.Stage.Provinces;
            }
        }

        public IEnumerable<IEvent> Events
        {
            get
            {
                return this.Stage.Events;
            }
        }

        public IEnumerable<IAchievement> Achievements
        {
            get
            {
                return this.Stage.Achievements;
            }
        }

        public int Turn
        {
            get
            {
                return this.Stage.Turn;
            }
        }

        public IMaterials Production
        {
            get
            {
                return this.Stage.Production;
            }
        }

        public IMaterials Stock
        {
            get
            {
                return this.Stage.Stock;
            }
        }

        public IMaterials Capacity
        {
            get
            {
                return this.Stage.Capacity;
            }
        }

        public IEnumerable<IModifierInstance> Modifiers
        {
            get
            {
                return this.Stage.Modifiers;
            }
        }

        private System.Reactive.Subjects.Subject<IModel> stageChanged = new System.Reactive.Subjects.Subject<IModel>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public IObservable<IModel> StageChanged
        {
            get
            {
                return this.stageChanged;
            }
        }

        private IList<IProvince> allProvinces = new List<IProvince>();

        public IEnumerable<IProvince> AllProvinces
        {
            get
            {
                return this.allProvinces;
            }
        }

        private IList<IAchievement> allAchievements = new List<IAchievement>();

        public IEnumerable<IAchievement> AllAchievements
        {
            get
            {
                return this.allAchievements;
            }
        }

        private IModelStage prevStage = null;

        public async Task<bool> Next(IController controller)
        {
            IModelStage ms = this.stage;
            this.stage = await this.stage.Next(this.prevStage == ms ? null :this.prevStage, this, controller);
            this.prevStage = ms;

            // all props
            this.OnPropertyChanged("");

            this.stageChanged.OnNext(this);

            //await controller.OnStageChanged(this);

            return stage != null;
        }
    }
}
