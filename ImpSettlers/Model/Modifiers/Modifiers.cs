﻿using System;
using System.Diagnostics;

namespace ImpSettlers.Model.Modifiers
{
    public class StorageModifierInstance : ModifierInstanceBase, IStorageModifierInstance
    {
        public StorageModifierInstance(IModifier mod, object origin, Func<IModifierInstance, IModel, ModifierStatus> activator) : base(mod, origin, activator)
        {
        }
    }

    public class StorageModifier : ModifierBase<StorageModifierInstance>, IStorageModifier
    {
        public IStorage Delta
        {
            get; set;
        }

        public StorageModifier(string descr, IStorage delta, Func<IModifierInstance, IModel, ModifierStatus> defaultActivator = null) : base(descr, defaultActivator)
        {
            Debug.Assert(delta != null);

            this.Delta = delta;
        }

    }

    public static class DefaultMods
    {
         public static IModifier ProductionStone1 = new StorageModifier("Na stałe zwiększ produkcję o 1 Kamień.", new StorageImpl(production: new MaterialsImpl(stone: 1)));
    }
}