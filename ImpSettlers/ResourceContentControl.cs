﻿using ImpSettlers.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ImpSettlers
{
    public class ResourceContentControl : ContentControl
    {
        private string description = "";
        public string Description {
            get
            {
                return this.description;
            }

            set
            {
                this.description = value;
            }
        }

        private IMaterials materials;
        public IMaterials Materials
        {
            get
            {
                return this.materials;
            }

            set
            {
                int oldQty = this.Qty;
                this.materials = value;

                this.OnPropertyChanged(new DependencyPropertyChangedEventArgs(QtyProperty, oldQty, Qty));
            }
        }
        public string Material { get; set; }

        public int Qty
        {
            get
            {
                if (this.Materials != null && this.Material != "")
                {
                    return (int)this.Materials.GetType().GetProperty(this.Material).GetValue(this.Materials);
                }

                else return 0;
            }

            set
            {

            }
        }

        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(string), typeof(ResourceContentControl));
        public static readonly DependencyProperty MaterialsProperty = DependencyProperty.Register("Materials", typeof(IMaterials), typeof(ResourceContentControl));
        public static readonly DependencyProperty MaterialProperty = DependencyProperty.Register("Material", typeof(string), typeof(ResourceContentControl));
        public static readonly DependencyProperty QtyProperty = DependencyProperty.Register("Qty", typeof(int), typeof(ResourceContentControl));


        static ResourceContentControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ResourceContentControl),
                       new FrameworkPropertyMetadata(typeof(ResourceContentControl)));
        }
    }
}
