﻿using ImpSettlers.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImpSettlers
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class SelectFactionDlg : Window
    {
        private ICollection<IFaction> factions = new List<IFaction>();

        private IFaction faction;
        public IFaction Faction
        {
            get { return this.faction;  }
            set {
                if (value == null) throw new Exception("Required");
                this.faction = value;
                this.btnOk.IsEnabled = true;
            }
        }
        public ICollection<IFaction> Factions
        {
            get
            {
                return this.factions;
            }
            set
            {
                this.factions = value;
                this.cbFaction.ItemsSource = this.factions;
            }
        }

        public SelectFactionDlg()
        {
            InitializeComponent();
            this.cbFaction.DataContext = this;
            this.btnOk.IsEnabled = false;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_Error(object sender, ValidationErrorEventArgs e)
        {
            MessageBox.Show("Należy wybrać frakcję");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //this.cbFaction.GetBindingExpression(ComboBox.SelectedItemProperty).UpdateSource();
            //txtName.GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }
    }
}
